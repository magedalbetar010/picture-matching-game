import 'package:flutter/material.dart';
import 'dart:math';
class GameApp extends StatefulWidget {
  const GameApp({Key? key}) : super(key: key);

  @override
  _GameAppState createState() => _GameAppState();
}

class _GameAppState extends State<GameApp> {
  int leftImageNumerR = 1;
  int leftImageNumerl =2;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.indigo,
          appBar: AppBar(
            backgroundColor: Colors.indigo[800],
            title: Text('تطابق الصورة'),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                leftImageNumerR==leftImageNumerl?'مبروك لقد نجحت':
                'حاول مرة آخرى',
                style: TextStyle(color: Colors.white, fontSize: 42),
              ),
              Row(
                children: [
                  Expanded(
                    child:  TextButton(onPressed: () {
                      setState(() {
                     leftImageNumerR=Random().nextInt(8)+1;
                      });
                    },
                        child: Image.asset('images/image-$leftImageNumerR.png')),
                  ),
                  Expanded(
                    child:  TextButton(onPressed: () {
                      setState(() {
                        leftImageNumerl=Random().nextInt(8)+1;
                      });
                    },
                        child: Image.asset('images/image-$leftImageNumerl.png')),
                  ),

                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
